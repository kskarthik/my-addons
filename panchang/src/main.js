var pDate = new Date();

init(pDate); 

new niceDatePicker({
        dom:document.getElementById('calendar-demo-wrapper'),
        mode:'en',
        onClickDate:function(date){

            pDate = date;
            
            init(pDate);
        } 

    });

function init(pDate) {

var t=  new Date(pDate);
  panchang.calculate(t, function() {
    document.getElementById("day").textContent=panchang.Day.name;
    
    document.getElementById("tithi").textContent=panchang.Tithi.name;
    document.getElementById("tithi-s").textContent=panchang.Tithi.start;
    document.getElementById("tithi-e").textContent=panchang.Tithi.end;
    
    document.getElementById("nakshtra").textContent=panchang.Nakshatra.name;
    document.getElementById("nakshatra-s").textContent=panchang.Nakshatra.start;
    document.getElementById("nakshatra-e").textContent=panchang.Nakshatra.end;
    
    document.getElementById("karna").textContent=panchang.Karna.name;
    document.getElementById("karna-s").textContent=panchang.Karna.start;
    document.getElementById("karna-e").textContent=panchang.Karna.end;
    
    document.getElementById("yoga").textContent=panchang.Yoga.name;
    document.getElementById("yoga-s").textContent=panchang.Yoga.start;
    document.getElementById("yoga-e").textContent=panchang.Yoga.end;
    
    document.getElementById("raasi").textContent=panchang.Raasi.name;
    document.getElementById("ayanamsa").textContent=panchang.Ayanamsa.name;
  });
  
  }
